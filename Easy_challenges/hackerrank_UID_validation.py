import string
import random
import sys


list_char_lower = list(string.ascii_lowercase)
list_char_upper = list(string.ascii_uppercase)


count = int(sys.stdin.readlines()[0])


while count  > 0:
    
    uid = ''
    uid = random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase)
    for i in range(10):
        next_letter = ''
        next_letter = random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase)
        if next_letter not in uid.lower() and next_letter not in uid.upper():
            uid += next_letter         
        else:
            i -=1

    count -= 1
    print (uid)