import sys

def transfer_to_set(obj):
    obj = (obj.split('\n')[0]).split(' ')
    return set([int(i) for i in obj if i.isalnum()])


def get_operation_set(lst):
    if lst:
        my_set = transfer_to_set(lst[1])
        my_operation = lst[0].split(' ')[0]
        return [my_operation,my_set]
    
def apply_changes_list(my_set, lst):

    for i in lst:
        if i:
            oper, opr_set = get_operation_set(i)
            getattr(my_set, oper)(opr_set)
    return my_set
    


data = sys.stdin.readlines()
index = 0
main_set = transfer_to_set(data[1])

del data[0:3]
index = 0
split_list = []
for y in range(len(data)):
    split_list.append(data[y*2:index+2])
    index += 2

main_set = apply_changes_list(main_set, split_list)
print(sum(main_set))

