from html.parser import HTMLParser
import sys

class MyHTMLParser(HTMLParser):

    def handle_comment(self, data):
        if len(data.split('\n')) > 1:
            print('>>> Multi-line Comment\n%s'%data)
        else:
           print('>>> Single-line Comment\n%s'%data)
    
    def handle_data(self, data):
        if data != '\n':
            print (">>> Data\n%s"%(data))


data = sys.stdin.readlines()
to_parse_data=''
del data[0]
for i in data:
    to_parse_data += i

parser = MyHTMLParser()
parser.feed(to_parse_data)




