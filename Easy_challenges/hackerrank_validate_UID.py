import re

for _ in range(int(input())):
    uid = ''.join(sorted(input()))
    try:
        # has 2 upper case
        assert re.search(r'[A-Z]{2}', uid)
        # has 3 digits
        assert re.search(r'\d{3}', uid)
        # has 10 chars
        assert re.search(r'^[a-zA-Z0-9]{10}',uid)
        # nothing is occured more than once like No AA or 22 but it can have Aa as one is cap 
        assert not re.search(r'(.)\1', uid)
        assert len(u) == 10
    except:
        print('Invalid')
    else:
        print('Valid')