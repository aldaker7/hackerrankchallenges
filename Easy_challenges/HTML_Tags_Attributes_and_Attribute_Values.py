from html.parser import HTMLParser
import sys


class MyHTMLParser(HTMLParser):

    def get_attrs_in_tag(self, attrs):
        for i in attrs:
            for n in i:
                if i.index(n) == 0:
                    print('-> %s > %s'%(n,i[i.index(n)+1]))
    
    def handle_starttag(self, tag, attrs):
        print(tag)
        if attrs:
            self.get_attrs_in_tag(attrs)
  
    
data = sys.stdin.readlines()
to_parse_data=''
del data[0]
for i in data:
    to_parse_data += i

parser = MyHTMLParser()
parser.feed(to_parse_data)




