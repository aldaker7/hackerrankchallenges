import sys
def check_if_string_valid(listy,colorstr):
    for i in colorstr:
        if i.lower() not in listy:
            return False
    return True

def split_list(list_to_loop, split_item):
    result_list = []
    for i in list_to_loop:
        for n in (i.split(split_item)):
            result_list.append(n)
    return result_list

data = sys.stdin.readlines()   
count = 0
css_chars = ['a','b','c','d','e','f','0','1','2','3','4','5','6','7','8','9']
final_list = []
listy=[]
for i in data:
    if '#' in i:
        if bool(data[count+1]):
            if '{' not in data[count+1]:
                new_array = (i.replace('#', ',,#,,'))
                for n in new_array.split(',,'):
                    for h in (n.split(';')):
                        final_list.append(h)
    count += 1

listy = split_list(final_list, ',')
final_list = []
final_list = split_list(listy,')')

count = 0
for i in final_list:
    if i == '#':
        if len(final_list[count+1]) in [3,6]:
            if check_if_string_valid(css_chars,final_list[count+1]):
                print(i+final_list[count+1])
    count += 1