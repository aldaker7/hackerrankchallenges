# valid should be
# range 100000 to 999999
# no alternate repeating such 525 5 is alternate repeting

import re

for _ in range(int(input())):
    postal_code = input()

    try:
        
        assert re.search(r'[1-9][0-9]{5}',postal_code)
        assert not re.search(r'(\d)(?=\d\1)',postal_code)

    except:
        print(False)

    else:
        print(True)



    