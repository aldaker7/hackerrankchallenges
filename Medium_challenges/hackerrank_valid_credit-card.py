import re


for _ in range(int(input())):
    cc = input()
    
    try:
        
        assert re.search(r'^[4-6]',cc)
        if '-' in cc:
            assert re.search(r'^\d{4}-\d{4}-\d{4}-\d{4}$',cc)
            assert not bool(re.search(r'(.)\1{3}',''.join(cc.split('-'))))
        else:
            assert re.search(r'[0-9]{16}',cc)
            assert not bool(re.search(r'(.)\1{3}',cc))
       
    except:
        print('Invalid')
    else:
        print('Valid')