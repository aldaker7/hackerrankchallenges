import re, sys

# starts with 6 small char and followed by optional _ and if _ exist followed by optional digits [0-4] times

def check_email(email):
    pattern = r'^([a-z]{1,6}(\_\d{0,4})?)@hackerrank.com$'
    if bool(re.search(pattern, email)):
        return 'Valid'
    return 'Invalid'


email = input()
print(check_email(email))


