
import sys
import re

# getting the first of the sentence in case it has specialChar
def get_first_sent(lst):
    sent = ''
    is_special = True
    for i in lst:
        while is_special:
            for n in i:
                while not n.isalnum():
                    sent += n
                    break
                else:
                    is_special = False
                    break
            break
    return sent

# getting the last of the sentence in case it has specialChar
def get_last_sent(lst):
    sent = ''
    is_special = True
    for i in lst:
        while is_special:
            for n in i[::-1]:
                while not n.isalnum():
                    sent += n
                    break
                else:
                    sent = sent[::-1]
                    is_special = False
                    break
            break
    
    return sent

# getting the words when there is no special char
def get_mid_sent(lst):
    mid_sen = ''
    for i in lst:
        for n in i:
            try:
                assert n.isalnum()
            except:
                mid_sen += ' '
            else:
                mid_sen += n
    return mid_sen.replace('  ',' ')

# reading data
data = sys.stdin.readlines()

# getting array indexs
number_of_item = int(data[0][0:1])
number_of_i_in_item = int(data[0][1:])

# deleting the first number inserted as they are not related
del data[0]

d = []
word=  ''
index = 0

# arranging the data in right order in a list
for item in data:
    try:
        assert not d
    except:
        for n in range(number_of_i_in_item):
            d[n] += item[n]
    else:
        while index != number_of_i_in_item:
            d.append(item[index])
            index += 1

# getting reverse list of arranged data to use in the last sentence
reverse_list = [i for i in d]
reverse_list.reverse()
last = ''
try:
    # to check if it has any alnum in the list otherwise we don't need to use it as it will duplicate the special char.
    assert re.search(r'[A-z]|[0-9]',''.join(reverse_list))
except:
    last = ''
else:
    last = get_last_sent(reverse_list)
first = get_first_sent(d)
mid = get_mid_sent(d)

print(first+(mid.strip())+last)
        
